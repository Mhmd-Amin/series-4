package sbu.cs.parser.json;

public class KeyFilter {
    private String key;
    protected KeyFilter(String rawKey) {
        filter(rawKey);
    }

    private void filter(String rawKey) {
        int firstDoubleQut = -1, endDoubleQut = -1;
        for (int i = 0; i < rawKey.length(); i++) {
            if (firstDoubleQut == -1 && rawKey.charAt(i) == '\"') {
                firstDoubleQut = i;
                endDoubleQut = i;
            }
            else if (endDoubleQut != -1 && rawKey.charAt(i) == '\"') {
                endDoubleQut = i;
            }
        }
        this.key = rawKey.substring(firstDoubleQut + 1, endDoubleQut);
    }

    protected String getKeyFilter() {
        return this.key;
    }
}