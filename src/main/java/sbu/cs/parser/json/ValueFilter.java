package sbu.cs.parser.json;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValueFilter {
    private String value;

    protected ValueFilter(String rawValue) {
        this.value = rawValue;
        filter();
    }

    private void removeQut() {
        int firstQuot = -1, endQuot = -1;
        for (int i = 0; i < this.value.length(); i++) {
            if (firstQuot == -1 && this.value.charAt(i) == '\"') {
                firstQuot = i;
                endQuot = i;
            }
            else if(endQuot != -1 && this.value.charAt(i) == '\"') {
                endQuot = i;
            }
        }
        if (firstQuot != endQuot) {
            this.value = this.value.substring(firstQuot + 1, endQuot);
        }
    }


    private void filter() {
        String correctValuePattern;
        correctValuePattern = "(\\[(\\s*\\d+,*\\s*)*\\]|(\"{1}((\\s)*(\\w)*(\\s)*)*\"{1})|(\\s*\\d+\\s*)|(null|true|false){1})";
        Pattern valuePattern = Pattern.compile(correctValuePattern);
        Matcher valueMatcher = valuePattern.matcher(this.value);
        if (valueMatcher.find() == true) {
            this.value = valueMatcher.group();
            removeQut();
        } else {
            this.value = "";
        }
    }

    protected String getValueFilter() {
        return this.value;
    }
}