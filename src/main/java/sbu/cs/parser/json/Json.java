package sbu.cs.parser.json;

public class Json implements JsonInterface {
    private String[] key;
    private String[] value;
    protected Json(String data) {
        Splitter splitter = new Splitter(data);
        this.key = splitter.getRawKeyArr().clone();
        this.value = splitter.getRawValueArr().clone();
        for (int i = 0; i < this.value.length; i++) {
            KeyFilter keyFilter = new KeyFilter(this.key[i]);
            this.key[i] = keyFilter.getKeyFilter();
            ValueFilter valueFilter = new ValueFilter(this.value[i]);
            this.value[i] = valueFilter.getValueFilter();
        }
    }

    private int indexOf(String key) {
        for (int i = 0; i < this.key.length; i++) {
            if (this.key[i].equals(key)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public String getStringValue(String key) {
        // TODO implement this
        int index = indexOf(key);
        if (index != -1) {
            return this.value[index];
        }
        else {
            System.out.println("not find");
            return null;
        }
    }
}
