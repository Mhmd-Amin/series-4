package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Splitter {
    private String[] value;
    private String[] key;
    private String text;
    private List<String> keyValue = new ArrayList<>();
    protected Splitter(String data) {
        this.text = data;
        keyValueSplitter();
    }

    private void keyValueSplitter() {
        boolean isArray = false;
        boolean isCurlyBrace = false;
        for(int i = 0, j = 0; i < this.text.length(); i++) {
            if (this.text.charAt(i) == '{' && isCurlyBrace == false) {
                isCurlyBrace = true;
                j = i;
            }
            else if (this.text.charAt(i) == '[' && isArray == false) {
                isArray = true;
            } else if (this.text.charAt(i) == ']' && isArray == true) {
                isArray = false;
            } else if (this.text.charAt(i) == ',' && isArray == false) {
                this.keyValue.add(this.text.substring(j + 1, i));
                j = i;
            } else if (this.text.charAt(i) == '}') {
                this.keyValue.add(this.text.substring(j + 1, i));
            }
        }
        KeyValueSeparator keyValueSeparator = new KeyValueSeparator(this.keyValue);
        this.value = keyValueSeparator.getRawValueArr().clone();
        this.key = keyValueSeparator.getRawKeyArr().clone();
    }

    protected String[] getRawValueArr() {
        return this.value;
    }

    protected String[] getRawKeyArr() {
        return this.key;
    }
}


