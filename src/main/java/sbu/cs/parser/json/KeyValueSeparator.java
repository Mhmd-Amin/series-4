package sbu.cs.parser.json;

import java.util.List;

public class KeyValueSeparator {
    private String[] rawKeyArr;
    private String[] rawValueArr;
    protected KeyValueSeparator(List<String> listRawKeys) {
        keySeparator(listRawKeys);
    }

    protected void keySeparator(List<String> listRawKeys) {
        this.rawKeyArr = new String[listRawKeys.size()];
        this.rawValueArr = new String[listRawKeys.size()];
        for (int i = 0; i < listRawKeys.size(); i++) {
            String[] tempKeyValue;
            tempKeyValue = listRawKeys.get(i).split(":", 2);
            this.rawKeyArr[i] = tempKeyValue[0];
            this.rawValueArr[i] = tempKeyValue[1];
        }
    }

    protected String[] getRawKeyArr() {
        return this.rawKeyArr;
    }

    protected String[] getRawValueArr() { return this.rawValueArr; }
}


