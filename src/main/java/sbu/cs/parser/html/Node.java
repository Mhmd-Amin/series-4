package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Node implements NodeInterface {
        private List<Node> childTag = new ArrayList<>();
        private String contents;
        private String headTagName;
        private Map<String, String> tagAttributeMap = new HashMap<>();
        private final String tagPattern = "<(\\w+\\d*){1}";

        public Node(String html) {
            Tag tags = new Tag(html);
            this.contents = tags.getTagData();
            this.headTagName = tags.getTagName();
            if(!isExistAttribute(html)) {
                TagAttribute tagAttribute = new TagAttribute(html, this.headTagName);
                this.tagAttributeMap.putAll(tagAttribute.getTagAttributeMap());
            }
            if (isExistChildTag()) {
                makeSubNode();
            }
        }
        private Boolean isExistAttribute(String html) {
            Pattern tagAttributePat = Pattern.compile("(<" + this.headTagName + ">){1}");
            Matcher tagAttributeMatcher = tagAttributePat.matcher(html);
            return tagAttributeMatcher.find();
        }

        private Boolean isExistChildTag() {
            Pattern tagPat = Pattern.compile(this.tagPattern);
            Matcher tagPatMatcher = tagPat.matcher(this.contents);
            return tagPatMatcher.find();
        }

        private void makeSubNode() {
            Node subNode = new Node(this.contents);
            this.childTag.add(subNode);
        }
    /*
    * this function will return all that exists inside a tag
    * for example for <html><body><p>hi</p></body></html>, if we are on
    * html tag this function will return <body><p1>hi</p1></body> and if we are on
    * body tag this function will return <p1>hi</p1> and if we are on
    * p tag this function will return hi
    * if there is nothing inside tag then null will be returned
     */
    @Override
    public String getStringInside() {
        // TODO implement this
        return this.contents;
    }

    /*
    *
     */
    @Override
    public List<Node> getChildren() {
        return this.childTag;
    }

    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        // TODO implement this
        return null;
    }
}