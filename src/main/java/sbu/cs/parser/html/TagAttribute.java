package sbu.cs.parser.html;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TagAttribute {
    private String tag;
    private String startTagAttribute;
    private String tagName;
    private String[] RawTagAttributeArr;
    private Map<String, String> tagAttributeMap = new HashMap<>();

    public TagAttribute(String tag, String tagName) {
        this.tag = tag;
        this.tagName = tagName;
        if (isExistAttribute()) {
            this.RawTagAttributeArr = findTagAttribute();
            for (String keyValue : RawTagAttributeArr) {
                Json json = new Json(keyValue);
                this.tagAttributeMap.put(json.getKey(), json.getValue());
            }
        }
    }

    private boolean isExistAttribute() {
        Pattern tagAttributePat = Pattern.compile("(<" + this.tagName + "(.+)>{1}){1}");
        Matcher tagAttributeMatcher = tagAttributePat.matcher(tag);
        if (tagAttributeMatcher.find()) {
            this.startTagAttribute = tagAttributeMatcher.group();
            return true;
        }
        return false;
    }

    private String[] findTagAttribute() {
        String[] tagAttrArr;
        Pattern tagAttributePat = Pattern.compile("(\\w+)=\"(.+)\"");
        Matcher tagAttributeFinder = tagAttributePat.matcher(this.startTagAttribute);
        if (tagAttributeFinder.find()) {
            tagAttrArr = tagAttributeFinder.group().split(" ");
            return tagAttrArr;
        }
        return null;
    }

    public Map<String, String> getTagAttributeMap() {
        return this.tagAttributeMap;
    }


}

