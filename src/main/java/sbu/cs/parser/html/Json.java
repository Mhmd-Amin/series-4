package sbu.cs.parser.html;

public class Json {
    private String rawKeyValue;
    private String key;
    private String value;
    public Json(String keyValue) {
        this.rawKeyValue = keyValue;
        keyValueSplitter();
    }

    private void keyValueSplitter() {
        String[] tempStr;
        tempStr = this.rawKeyValue.split("=", 2);
        this.key = tempStr[0];
        this.value = valueFilter(tempStr[1]);
    }

    private String valueFilter(String strKey) {
        return strKey.substring(1, strKey.length() - 1);
    }

    public String getKey() {
        return this.key;
    }

    public String getValue() {
        return this.value;
    }
}

