package sbu.cs.parser.html;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tag {
    private String headName;
    private String tagData;
    private String data;
    private final String headTagPattern = "<(\\w+\\d*){1}";
    public Tag(String data) {
        this.data = data;
        this.headName = tagName();
        this.tagData = findTagAttribute();
    }

    private String tagName() {
        Pattern headTag = Pattern.compile(this.headTagPattern);
        Matcher headTagMatcher = headTag.matcher(this.data);
        if (headTagMatcher.find()) {
            return headTagMatcher.group();
        }
        return null;
    }

    private int findEndOpenTagPos() {
        int endOpenTagPos = 0;
        String endTagPat = ">{1}";
        Pattern endTag = Pattern.compile(endTagPat);
        Matcher headEndTagMatcher = endTag.matcher(this.data);
        if (headEndTagMatcher.find()) {
            endOpenTagPos = headEndTagMatcher.end();
            return endOpenTagPos;
        }
        return endOpenTagPos;
    }

    private String findTagAttribute() {
        String tagNameEnd = "</(" + this.headName.substring(1) + "){1}";
        Pattern headTagEnd = Pattern.compile(tagNameEnd);
        Matcher headTagEndMatcher = headTagEnd.matcher(this.data);
        if (headTagEndMatcher.find()) {
            return this.data.substring(findEndOpenTagPos(), headTagEndMatcher.start());
        }
        return null;
    }

    public String getTagName() {
        return this.headName.substring(1);
    }

    public String getTagData() {
        return this.tagData;
    }
}